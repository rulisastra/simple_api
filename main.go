package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Album struct {
	Id      string `json:"Id"`
	Title   string `json:"Title"`
	Desc    string `json:"Desc"`
	Content string `json:"Content"`
}

// struktur Album
var Albums []Album

// untuk homepage
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to Taylor Swift Page!")
	fmt.Println("Endpoint Hit: homePage")
}

// mengambil semua Album
func returnAllAlbums(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllAlbums")
	json.NewEncoder(w).Encode(Albums)
}

// returnSingleAlbums
func returnSingleAlbum(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	for _, Album := range Albums {
		if Album.Id == key {
			json.NewEncoder(w).Encode(Album)
		}
	}
}

func createNewAlbum(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var Album Album
	json.Unmarshal(reqBody, &Album)
	Albums = append(Albums, Album)
	json.NewEncoder(w).Encode(Album)
}

func deleteAlbum(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	for index, Album := range Albums {
		if Album.Id == id {
			Albums = append(Albums[:index], Albums[index+1:]...)
		}
	}
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/Albums", returnAllAlbums)
	myRouter.HandleFunc("/Album", createNewAlbum).Methods("POST")
	myRouter.HandleFunc("/Album/{id}", deleteAlbum).Methods("DELETE")
	myRouter.HandleFunc("/Album/{id}", returnSingleAlbum)

	log.Fatal(http.ListenAndServe(":10001", myRouter))
}

func main() {
	Albums = []Album{
		Album{Id: "1", Title: "Taylor Swift", Desc: "release in 2006", Content: "15 Songs"},
		Album{Id: "2", Title: "Fearless", Desc: "release in 2008", Content: "19 Songs"},
		Album{Id: "3", Title: "Speak Now", Desc: "release in 2010", Content: "14 Songs"},
		Album{Id: "4", Title: "Red", Desc: "release in 2012", Content: "22 Songs"},
		Album{Id: "5", Title: "1989", Desc: "release in 2014", Content: "13 Songs"},
		Album{Id: "6", Title: "Reputatuon", Desc: "release in 2017", Content: "15 Songs"},
		Album{Id: "7", Title: "Lover", Desc: "release in 2019", Content: "18 Songs"},
		Album{Id: "8", Title: "folklore", Desc: "release in 2020", Content: "17 Songs"},
	}

	handleRequests()
}
